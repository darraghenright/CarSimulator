package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import model.Race;
import view.RaceView;

public class RaceController
{
    private static final String FILE_LOCATION = "./race.dat";

    public RaceController()
    {
        RaceView view = new RaceView();
        Race lastRace = this.getLastRace();

        if (lastRace != null) {
            view.showResult(lastRace);
        }

        Race race = new Race(view.getDriverNames());

        race.start();
        
        view.showResult(race);
        this.saveRace(race);
    }

    public Race getLastRace()
    {
        try {
            File file = new File(FILE_LOCATION);

            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Race lastRace = (Race) ois.readObject();

                return lastRace;
            }
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
	}

    public void saveRace(Race race)
    {
        try {
            File file = new File(FILE_LOCATION);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            file.createNewFile();
            oos.writeObject(race);
            oos.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
