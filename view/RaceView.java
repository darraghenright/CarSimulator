package view;

import model.Car;
import model.Race;
import java.util.ArrayList;
import java.util.Scanner;

public class RaceView
{
    public ArrayList<String> getDriverNames()
    {
        System.out.print("How many cars are entering the race: ");

        Scanner keyboard = new Scanner(System.in);
        int numberOfDrivers = keyboard.nextInt();
        ArrayList<String> driverNames = new ArrayList<String>();

        for (int i = 0; i < numberOfDrivers; i++) {
            System.out.print("What is the name of the driver: ");
            String driverName = keyboard.next();
            driverNames.add(driverName);
        }

        return driverNames;
    }

    public void showResult(Race race)
    {
        System.out.println("=============================================");
        System.out.println("Race results for " + race.date.toString());
        System.out.println("=============================================");
        System.out.println(race.winner.driverName + " won the race!");
        System.out.println("The race took " + race.seconds + " seconds");

        for (Car car : race.cars) {
            System.out.println(car.driverName + " drove " + car.distanceTravelled + " meters");
        }

        System.out.println("---------------------------------------------");
    }
}
