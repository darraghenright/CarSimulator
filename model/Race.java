package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Race implements Serializable
{
    public ArrayList<Car> cars;
    public Car winner;
    public int seconds = 0;
    public Date date;

    public Race(ArrayList<String> driverNames)
    {
        this.cars = new ArrayList<Car>();

        for (String driverName : driverNames) {
            this.cars.add(new Car(driverName));
        }
    }

    public void start()
    {
        int RACE_COURSE = 10000;

        date = new Date();

        while (this.winner == null) {
            this.seconds++;
            for (Car car : this.cars) {
                car.drive();
                if (car.distanceTravelled >= RACE_COURSE) {
                    this.winner = car;
                }
            }
        }
    }
}
