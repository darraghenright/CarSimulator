package model;

import java.io.Serializable;
import java.util.Random;

public class Car implements Serializable
{
    public final int MAX_DISTANCE_PER_SECOND = 50;
    public int distanceTravelled = 0;
    public String driverName;

    public Car(String driverName)
    {
        this.driverName = driverName;
    }

    public void drive()
    {
        Random random = new Random();
        int distanceAdded = random.nextInt(MAX_DISTANCE_PER_SECOND) + 1;
        this.distanceTravelled += distanceAdded;
    }
}
